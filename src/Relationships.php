<?php
/**
 * Created by PhpStorm.
 * User: mcalvo
 * Date: 10/08/2018
 * Time: 13:10
 */

namespace Engeni\Aspects;

use ErrorException;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Str;
use ReflectionClass;
use ReflectionMethod;
use Illuminate\Support\Collection;

class Relationships
{
    private $model;
    private $relationships;

    public function __construct($model) {
        $this->model = $model;
        $this->relationships = null;
        $this->ignoredPublicMethods = $this->ignoredPublicMethods();
    }

    public function __invoke($key = false)
    {
        if (!$this->relationships)
            $this->all();

        if ($key)
            return $this->byKey($key);
        else
            return $this->relationships;
    }

    public function ignoredPublicMethods()
    {
        return collect(['boot', 'forceDelete', 'bootSoftDeletes', 'restore', 'trashed',
            'isForceDeleting', 'getDeletedAtColumn', 'getQualifiedDeletedAtColumn',
            'delete', 'create', 'save', 'update', 'beforeValidation']);
    }

    public function getMethodBody($anReflectionMethodInstance){
        $func = $anReflectionMethodInstance;
        $f = $func->getFileName();
        $start_line = $func->getStartLine() - 1;
        $end_line = $func->getEndLine();
        $length = $end_line - $start_line;
        $source = file($f);
        $source = implode('', array_slice($source, 0, count($source)));
        $source = preg_split("/(\n|\r\n|\r)/", $source);
        $body = '';
        for($i=$start_line; $i<$end_line; $i++)
            $body.="{$source[$i]}\n";

        return $body;
    }

    public function methodIsAEloquentRelation($anReflectionMethodInstance)
    {
        $body = $this->getMethodBody($anReflectionMethodInstance);
        return Str::contains($body, $this->allRelations());
    }

    public function allRelations(){
        return ['hasOne', 'belongsTo', 'hasMany', 'belongsToMany', 'hasManyThrough', 'morphTo', 'morphMany', 'morphedByMany'];
    }

    public function all() {

        $this->relationships = collect([]);

        foreach((new ReflectionClass($this->model))->getMethods(ReflectionMethod::IS_PUBLIC) as $method)
        {
            if ($method->class == get_class($this->model)
                && empty($method->getParameters())
                && $method->getName() !== __FUNCTION__
                && $method->isPublic()
                && !$this->ignoredPublicMethods->contains($method->getName())
                && $this->methodIsAEloquentRelation($method))
            {

                try {
                    $return = $method->invoke($this->model);

                    if ($return instanceof Relation)
                    {
                        $ownerKey = null;
                        if ((new ReflectionClass($return))->hasMethod('getOwnerKey'))
                            $ownerKey = $return->getOwnerKey();
                        else
                        {
                            $segments = explode('.', $return->getQualifiedParentKeyName());
                            $ownerKey = $segments[count($segments) - 1];
                        }

                        $rel = new Relationship([
                            'name' => $method->getName(),
                            'type' => (new ReflectionClass($return))->getShortName(),
                            'model' => (new ReflectionClass($return->getRelated()))->getName(),
                            'foreignKey' => (new ReflectionClass($return))->hasMethod('getForeignKey')
                                ? $return->getForeignKey()
                                : $return->getForeignKeyName(),
                            'ownerKey' => $ownerKey,
                        ]);

                        $this->relationships[$rel->name] = $rel;
                    }
                } catch(\Throwable $e) {}

            }
        }

        return $this->relationships;
    }

    public function byKey($key)
    {
        $relationships = new Collection;

        foreach ($this->relationships as $name => $relationship)
            if ($relationship->type == 'BelongsTo'
                && $relationship->foreignKey == $key)
                $relationships[$name] = $relationship;

        return $relationships;
    }
}