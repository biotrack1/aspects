<?php
/**
 * Created by PhpStorm.
 * User: mcalvo
 * Date: 09/08/2018
 * Time: 15:23
 */

namespace Engeni\Aspects\Traits;

trait APIResourceHelper
{

    public function aspects()
    {
        return $this->sendAspects($this->builder());
    }

    public function builder()
    {
        $class = $this->resourceModel();
        return (new $class)->builder();
    }

    public function sendAspects($aspects)
    {
        return response()->json(
            $aspects,
            200,
            ['Content-Type' => 'application/json', 'charset' => 'utf-8', 'Access-Control-Allow-Origin' => '*'],
            JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    }

    public function resourceModel()
    {
        preg_match('/([a-zA-Z]*)Controller/', class_basename($this), $matches);
        $class = $matches[1];
        $namespace = str_replace('\Http', '', get_class($this));
        $namespace = str_replace($class.'Controller', '', $namespace);
        $namespace = str_replace('Controllers', 'Models', $namespace);
        return ($namespace.$class);
    }
}