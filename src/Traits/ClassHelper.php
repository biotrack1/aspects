<?php
/**
 * Created by PhpStorm.
 * User: mcalvo
 * Date: 09/08/2018
 * Time: 15:23
 */

namespace Engeni\Aspects\Traits;

use App\Models\Store\Customer;
use Engeni\Aspects\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

trait ClassHelper
{
    use Helper;

    public function defaultBuilderClass()
    {
        return Builder::class;
    }

    public function lookupBuilderClass()
    {
        $customBuilderClass = $this->classNamespace() . '\\' . $this->className() . 'Builder';
        if(class_exists($customBuilderClass))
        {
            return $customBuilderClass;
        }
        return $this->defaultBuilderClass();
    }

    public function builderClass()
    {
        return $this->lookupBuilderClass();
    }

    public function builder()
    {
        $builderCached = $this->getBuilderFromCache();

        if($builderCached)
        {
            return $builderCached;
        }

        $builderClass = $this->builderClass();
        $builder = (new $builderClass($this));
        $this->setCacheBuilder($builder);
        return $builder;
    }

    private function setCacheBuilder($builder)
    {
        Cache::forever($this->cacheKey(), $builder);
    }

    private function getBuilderFromCache()
    {
        if(empty(Builder::$builderCache[$this->cacheKey()]))
        {
            Builder::$builderCache[$this->cacheKey()] = Cache::get($this->cacheKey());
        }
        return Builder::$builderCache[$this->cacheKey()];
    }

    private function cacheKey()
    {
        $model = (String)$this->thisClass();
        return "{$model}::builder()";
    }

    public function getImplicitRules()
    {
        return $this->rules;
    }

    public function getValidationRules()
    {
        $validations = [];
        foreach($this->builder()->validations as $attribute => $values)
        {
            if($this->exists) {
                foreach ($values as $key => $value) {
                    if (Str::contains($value, 'unique')) {
                        $values[$key] = "{$value},{$this->id}";
                    }
                }
            }
            $validations[$attribute] = implode('|', $values);
        }
        $validations = array_merge($validations, $this->getImplicitRules());

        if($this->exists) {
            foreach ($validations as $validation => $rule) {
                if (Str::contains($rule, 'unique_with')) {
                    $validations[$validation] = "{$rule},{$this->id}";
                }
            }
        }

        return $validations;
    }

    public function getFillable()
    {
        if (Builder::$alreadyBuildAspects) return [];
        return $this->builder()->getFillable();
    }

    public function getTableColumns()
    {
        return $this->builder()->getTableColumnsArray();
    }

    public function getAssociations()
    {
        return $this->builder()->associations();
    }

    public function runDeleteActionsOnRelations()
    {
        $associationsToCheck = $this->getAssociations()->filter(function($asc)
        {
            return $asc->options->has('on_delete');
        });
        foreach ($associationsToCheck as $assName => $asc) {

            $opt = $asc->options->get('on_delete');

            if($opt == 'restrict')
            {
                $count = $this->$assName()->count();
                if($count > 0)
                {
                    $this->addError($assName, "You can not delete this model since you have {$count} associated models ");
                    return false;
                }
            }
            if($opt == 'cascade' && $this->isValid())
            {
                $toDelete = $this->{$assName};

                if($toDelete instanceof Collection)
                {
                    $toDelete->each(function($obj) use ($assName){
                        if(!$obj->delete())
                        {
                            $this->addError($assName, $obj->errors);
                            return false;
                        }
                    });
                }else{
                    if($toDelete && !$toDelete->delete())
                    {
                        $this->addError($assName, $toDelete->errors);
                        return false;
                    }
                }
            }
        }
    }

    public function delete()
    {
        try {
            DB::beginTransaction();
            $this->runDeleteActionsOnRelations();
            if($this->isValid()) {
                if (parent::delete()) {
                    DB::commit();
                    return true;
                }
                return false;
            }
            return false;
        } catch (Exception $e) {
            Log::exception($e);
        }
        DB::rollBack();
        return false;
    }

    public function getDates()
    {
        if(empty(Builder::$datesCache[get_class($this) . '_dates']))
        {
            $defaults = parent::getDates();
            $dates = array_unique(array_merge($defaults, $this->builder()->getDates()));
            Builder::$datesCache[get_class($this) . '_dates'] = $dates;
        }
        return Builder::$datesCache[get_class($this) . '_dates'];
    }

    public function displayAspect($aspect)
    {
        if(is_string($aspect))
        {
            $aspect = $this->builder()->aspects()[$aspect];
        }
        return $aspect->displayFor($this);
    }

    public function displayFor($object)
    {
        $aspect = $this->aspectNamed($this->displayString());
        if(!$aspect) return parent::__toString();
        return $object->displayAspect($aspect);
    }

    public function aspectNamed($name)
    {
        return $this->aspects()->get($name);
    }

    public function __toString(){
        return $this->displayFor($this);
    }

    public function displayString()
    {
        return $this->builder()->displayString();
    }

    public function aspects()
    {
        return $this->builder()->aspects();
    }

    public function hasColumn($column)
    {
        return in_array($column, $this->getTableColumns());
    }

}