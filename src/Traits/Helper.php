<?php
/**
 * Created by PhpStorm.
 * User: mcalvo
 * Date: 09/08/2018
 * Time: 15:23
 */

namespace Engeni\Aspects\Traits;

trait Helper
{

    public function eloquentModel()
    {
        return 'Illuminate\Database\Eloquent\Model';
    }

    public function isSubClassOf($object, $class)
    {
        return is_subclass_of($object, $class);
    }
    
    public function classNamespace()
    {
        return $this->classReflection()->getNamespaceName();
    }

    public function className()
    {
        return class_basename($this->thisClass());
    }

    public function classReflection()
    {
        return (new \ReflectionClass($this->thisClass()));
    }

    public function thisClass()
    {
        return get_called_class();
    }
    
}