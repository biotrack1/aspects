<?php
/**
 * Created by PhpStorm.
 * User: mcalvo
 * Date: 09/08/2018
 * Time: 16:13
 */
namespace Engeni\Aspects\Converters;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
class AssociationToText extends ObjectToText
{
    protected $association_class;
    protected $association_key;
    public function applyOptions()
    {
        parent::applyOptions();
        $this->association_class = $this->options->get('association_class');
        $this->association_key = $this->options->get('association_key', 'name');
        if (!$this->association_class) {
            throw new \Exception('Please provide an association_class and association_key');
        }
    }
    public function fromDisplay($text, $options = [])
    {
        $key = $this->association_key;
        $class = $this->association_class;
        $userId = app_context()->getUser()->id;
        $cacheKey = "{$class}_{$key}_userId_$userId";
        $expireIn = Carbon::now()->addSeconds(20);
        $objects = Cache::remember($cacheKey, $expireIn, function () use ($class, $key) {
            $query = $class::query();
            $scopes = $this->options->get('scopes', []);
            foreach ($scopes as $identifier=>$scopeClass) {
                $query->withGlobalScope($identifier, new $scopeClass);
            }
            return $query->get()->keyBy($key);
        });
        return $objects->get($text);
    }
    public function fromCsv($text, $options = [])
    {
        return parent::fromCsv($text, $options);
    }
    public function setValueFor($object, $column, $value)
    {
        $value = $value ? $value->id : null;
        return $object->{$column} = $value;
    }
}