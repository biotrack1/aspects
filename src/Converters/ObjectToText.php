<?php
/**
 * Created by PhpStorm.
 * User: mcalvo
 * Date: 09/08/2018
 * Time: 16:13
 */

namespace Engeni\Aspects\Converters;

use Illuminate\Support\Collection;

class ObjectToText
{
    protected $options;
    protected $null_string;

    public function __construct($options=Collection::class)
    {
        $this->options = $options;
        $this->applyOptions();
    }

    public function applyOptions()
    {
        $this->null_string =  $this->options->get('null_string', '');
    }

    public function nullString()
    {
        return $this->null_string;
    }

    public function displayFor($object)
    {
        return $object ? (string)$this->displayValueFor($object) : $this->nullString();
    }

    public function displayValueFor($object)
    {
        return $object;
    }

    public function fromCsv($text, $options=[])
    {
        if($text == '') return null;
        return $this->fromDisplay($text, $options);
    }

    public function fromDisplay($text, $options=[])
    {
        return $text;
    }

    public function setValueFor($object, $column, $value)
    {
        return $object->{$column} = $value;
    }
}