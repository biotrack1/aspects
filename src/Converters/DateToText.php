<?php
/**
 * Created by PhpStorm.
 * User: mcalvo
 * Date: 09/08/2018
 * Time: 16:13
 */

namespace Engeni\Aspects\Converters;

use Carbon\Carbon;
use Illuminate\Support\Arr;

class DateToText extends ObjectToText
{
    const DEFAULT_FORMAT = 'm/d/Y';

    public function fromDisplay($text, $options = [])
    {
        return Carbon::parse($text)->toDateString();
    }

    public function fromCsv($text, $options = [])
    {
        if ($text == $this->nullString()) return null;
        return Carbon::parse($text)->toDateString();
    }

    public function displayFor($object)
    {
        if (null !== $object) {
            $format = Arr::get($this->options, 'format', self::DEFAULT_FORMAT);
            return Carbon::parse($object)->format($format);
        }

        return $this->nullString();
    }
}