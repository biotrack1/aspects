<?php
/**
 * Created by PhpStorm.
 * User: mcalvo
 * Date: 09/08/2018
 * Time: 16:13
 */

namespace Engeni\Aspects\Converters;

class BooleanToText extends ObjectToText
{
    public function displayFor($boolean)
    {
        if($boolean) return trans('boolean.true');
        return trans('boolean.false');
    }

    public function fromDisplay($text, $options = [])
    {
        return $this->parse($text);
    }

    private function parse($text)
    {
        if(preg_match("/^(true|t|yes|y|1|si|sí|v)$/i", $text)) return true;
        if(preg_match("/^(false|f|no|n|0)$/i", $text)) return false;
        return false;
    }
}