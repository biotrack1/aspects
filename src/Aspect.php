<?php
/**
 * Created by PhpStorm.
 * User: mcalvo
 * Date: 09/08/2018
 * Time: 17:12
 */
namespace Engeni\Aspects;
use Engeni\Aspects\Converters\AssociationToText;
use Engeni\Aspects\Converters\BooleanToText;
use Engeni\Aspects\Converters\DateToText;
use Engeni\Aspects\Converters\IntegerToText;
use Engeni\Aspects\Converters\ObjectToText;
use Engeni\Aspects\Converters\StringToText;
class Aspect
{
    public $converter;
    # @param [Class] def_class in which is defined
    # @param [String] name for the Aspect
    # @param [Symbol] accessor used to read/write models
    # @param [Symbol] type for the Aspect
    # @param [Object] default value when nil
    # @param [Boolean] null whether is nullable
    public function __construct($defClass, String $name, String $accessor, $type='string', $default = null, $null = true, Array $options = [])
    {
        $this->def_class = $defClass;
        $this->name = $name;
        $this->accessor = $accessor;
        $this->type = $type;
        $this->default = $default;
        $this->null = $null;
        $this->options = collect($options);
        $this->setConverter();
    }
    public function valueFor($object)
    {
        if($value = $this->getValueFromMethod($object))
        {
            return $value;
        }
        if($value = $this->getValueFromAttributes($object))
        {
            return $value;
        }
        return $this->getValueFromProperty($object);
    }
    private function getValueFromProperty($object)
    {
        if($value = $object->{$this->accessor})
        {
            return $value;
        }
    }
    private function getValueFromMethod($object)
    {
        if(method_exists($object, $this->accessor))
        {
            $value = $object->{$this->accessor}();
            if(is_object($value) && $this->isARelation($value)){
                $value = $object->{$this->accessor};
            }
            return $value;
        }
    }
    private function isARelation($object)
    {
        return is_subclass_of($object, 'Illuminate\Database\Eloquent\Relations\Relation');
    }
    private function getValueFromAttributes($object)
    {
        if(method_exists($object, 'getAttribute') && $value = $object->getAttribute($this->accessor))
        {
            return $value;
        }
    }
    public function displayFor($object)
    {
        return $this->converter->displayFor($this->valueFor($object));
    }
    private function setConverter()
    {
        $converter = $this->getConverterTypes()->get($this->type, ObjectToText::class);
        $this->converter = new $converter($this->options);
    }
    private function getConverterTypes()
    {
        return collect([
            'integer' => IntegerToText::class,
            'string' => StringToText::class,
            'BelongsTo' => AssociationToText::class,
            'boolean' => BooleanToText::class,
            'date' => DateToText::class
        ]);
    }
    public function strongParam()
    {
        return $this->options->get('foreign_key', $this->name);
    }
    public function label()
    {
        return $this->options->get('label', $this->name);
    }
    # @return [Boolean] whether the receiver is visible
    public function isVisible()
    {
        return $this->options->get('visible', true);
    }
    # @return [Boolean] whether the receiver is editable
    public function isEditable()
    {
        return $this->options->get('editable', true);
    }
    public function isIdentifier()
    {
        return $this->options->get('identifier', false);
    }
    public function isRequired()
    {
        return !$this->null;
    }
    # @return [Boolean] whether the receiver is importable
    public function isImportable()
    {
        return $this->options->get('importable', false);
    }
    # @return [Boolean] whether the receiver is hidden
    public function isHidden()
    {
        return $this->options->get('hidden', false);
    }
    public function tab()
    {
        return $this->options->get('tab', 'Others');
    }
    # Set the receiver's options
    # @param [Array] options to be applied to the receiver and it's converter
    public function setOptions($options)
    {
        foreach ($options as $property => $option) {
            property_exists($this, $property) ? $this->$property = $option : $this->options = $this->options->merge($options);
        }
        $this->setConverter();
        return $this;
    }
    public function setPriority(Int $priority)
    {
        $this->options->put('priority', $priority);
        return $this;
    }
    public function importFromCsv($object, $text, $options=[])
    {
        $value = $this->converter->fromCsv($text, $options);
        $column = $this->strongParam();
        return $this->converter->setValueFor($object, $column, $value);
    }
    # Return a Collection Object.
    public function selectOptions($object=null)
    {
        try {
            if ($object && method_exists($object, "{$this->name}Options")) {
                return $object->{"{$this->name}Options"}();
            }
            if ($options = $this->options->get('in')) {
                return collect($options);
            }
            if ($this->isAssociation() && $associationClass = $this->options->get('association_class')) {
                $query = $associationClass::query();
                $scopes = $this->options->get('scopes', []);
                foreach ($scopes as $key=>$scopeClass) {
                    $query->withGlobalScope($key, new $scopeClass);
                }
                return $query->limit(10)->get()->map(function($opt){
                    return (string)$opt;
                });
            }
            return collect([]);
        }catch(\Throwable $e)
        {
            return collect([]);
        }
    }
    private function allRelations(){
        return collect(['HasOne', 'BelongsTo', 'HasMany', 'BelongsToMany', 'HasManyThrough', 'MorphTo', 'MorphMany', 'MorphedByMany', 'HasMany']);
    }
    private function isAssociation()
    {
        return $this->allRelations()->contains($this->type);
    }
}