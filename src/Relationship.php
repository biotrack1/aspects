<?php

namespace Engeni\Aspects;

use ErrorException;
use Illuminate\Database\Eloquent\Relations\Relation;
use ReflectionClass;
use ReflectionMethod;
use Illuminate\Support\Collection;

class Relationship
{
    public $name;
    public $type;
    public $model;
    public $foreignKey;
    public $ownerKey;
    public $options;

    public function __construct($relationship = [], array $options=[])
    {
        if ($relationship)
        {
            $this->name = $relationship['name'];
            $this->type = $relationship['type'];
            $this->model = $relationship['model'];
            $this->foreignKey = $relationship['foreignKey'];
            $this->ownerKey = $relationship['ownerKey'];
            $this->options = collect($options);
        }
    }

    public function setOptions($options)
    {
        foreach ($options as $property => $option) {
            property_exists($this, $property) ? $this->$property = $option : $this->options = $this->options->merge($options);
        }
        return $this;
    }
}