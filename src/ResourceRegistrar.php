<?php

namespace Engeni\Aspects;

use Illuminate\Routing\ResourceRegistrar as OriginalRegistrar;

class ResourceRegistrar extends OriginalRegistrar
{
    // add data to the array
    /**
     * The default actions for a resourceful controller.
     *
     * @var array
     */
    protected $resourceDefaults = ['aspects', 'index', 'create', 'store', 'show', 'edit', 'update', 'destroy'];


    /**
     * Add the data method for a resourceful route.
     *
     * @param  string  $name
     * @param  string  $base
     * @param  string  $controller
     * @param  array   $options
     * @return \Illuminate\Routing\Route
     */
    protected function addResourceAspects($name, $base, $controller, $options)
    {
        $uri = $this->getResourceUri($name).'/aspects';

        $action = $this->getResourceAction($name, $controller, 'aspects', $options);

        return $this->router->get($uri, $action);
    }
}