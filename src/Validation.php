<?php
/**
 * Created by PhpStorm.
 * User: mcalvo
 * Date: 01/11/2018
 * Time: 12:06
 */

namespace Engeni\Aspects;


class Validation
{
    protected $builder;
    protected $column;
    protected $reflections;
    protected $aspect;
    protected $indexes;

    public function __construct($builder, $column, $aspect)
    {
        $this->builder = $builder;
        $this->column = $column;
        $this->reflections = $builder->getColumnReflections();
        $this->indexes = $builder->getIndexes();
        $this->aspect = $aspect;
        $this->setValidations();
    }

    public function toArray()
    {
        return $this->validations;
    }
    
    public function aspect()
    {
        return $this->builder->aspects()[$this->column];
    }
    
    public function selectOptions()
    {
        return $this->aspect->selectOptions();
    }

    public function setValidations()
    {
        $validations = collect([]);
        if($this->isRequired() && $this->defaults() == null)
        {
            $validations->push('required');
            if($this->dataType()) {
                $validations->push($this->dataType());
            }
            if($this->length()){
                $validations->push('max:' . $this->length());
            }
            if($this->aspect->type == 'string' && $this->selectOptions()->isNotEmpty()){
                $validations->push('in:' . $this->selectOptions()->implode(','));
            }
        }else{
            $validations->push('sometimes');
            $validations->push('nullable');
            $validations->push($this->dataType());
            if($this->length()){
                $validations->push('max:' . $this->length());
            }
            if($this->aspect->type == 'string' && $this->selectOptions()->isNotEmpty()){
                $validations->push('in:' . $this->selectOptions()->implode(','));
            }
        }
        if($this->isUnique()){
            $validations->push("unique:{$this->tableName()},{$this->column}");
        }
        $this->validations = $validations->toArray();
    }

    private function isUnique(){
        $index = "{$this->tableName()}_{$this->column}_unique";
        return array_key_exists($index, $this->indexes);
    }

    private function tableName(){
        return $this->builder->tableName();
    }

    private function isRequired()
    {
        return $this->getColumnNotNull();
    }

    private function defaults()
    {
        return $this->reflections[$this->column]->getDefault();
    }

    private function dataType()
    {
        $dbType = $this->reflections[$this->column]->getType()->getName();
        $type = $this->validationTypeFor($dbType);
        $type = $this->validationTypesByColumnNames()->get($this->column, $type);
        return $type;
    }

    private function validationTypeFor($dbType)
    {
        if(array_key_exists($dbType, $this->validationTypes())){
            return $this->validationTypes()[$dbType];
        }
    }

    private function validationTypes()
    {
        return ['boolean' => 'boolean', 'date' => 'date', 'array' => 'string',
            'string' => 'string', 'float' => 'numeric',
            'text' => 'string', 'enum' => 'string',
            'integer' => 'integer', 'datetime' => 'datetime', 'time' => 'time'];

    }

    private function validationTypesByColumnNames()
    {
        return collect(['email' => 'email', 'contact_email' => 'email', 'url' => 'url']);
    }

    private function length()
    {
        return $this->reflections[$this->column]->getLength();
    }


    private function getColumnNotNull()
    {
        return $this->reflections[$this->column]->getNotNull();
    }

}