Forian Aspects is a aspect-oriented library in plain PHP with features for a new level of software development. The library allows cross-cutting issues to be solved in the traditional object-oriented PHP code by providing a highly efficient and transparent hook system for your exisiting code.

* Provides dynamic hook system for PHP without changes in the original source code.
* Doesn't require any PECL-extentions (php-aop, runkit, uopz) and DI-containers to work.
* Object-oriented design of aspects, joinpoints and pointcuts.
* Intercepting an execution of any public or protected method in a classes.
* Intercepting an execution of static methods and methods in final classes.
* Intercepting an execution of methods in the traits.
* Intercepting an access to the public/protected properties for objects.
* Integrates with Laravel Eloquent directly
* Gives Eloquent models a description of the database structure